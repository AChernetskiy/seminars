#include <iostream>
#include <vector>

class comp
{
	int real = 0;
	int imag = 0;
	std::vector<int> data;
public:
	comp() {}
	explicit comp(int x):real(x){}
	comp(int x, int y)
	{
		real = x;
		imag = y;
	}

	comp(const comp& other)
	{
		real = other.real;
		imag = other.imag;
	}

	double len() const
	{
		return sqrt(real * real + imag * imag);
	}

	// ��������!!!!!!!!!!!!
	// ��������� ::, . , sizeof, typeid
	// � ��� �� ��� ���������� �����
	// ����������� ������

	comp& operator = (const comp& other)
	{
		this->real = other.real;
		this->imag = other.imag;
	}
	comp& operator += (const comp& other)
	{
		this->real += other.real;
		this->imag += other.imag;
		return *this;
	}
	//comp operator + (const comp& other)
	//const ��������� ��� ������������ 2 ����������� ��������. ������� ������ ����� ����� const, �������� ������.
	// const ����� ������ ��������, ��� ����� �� �������� ���� ������

	bool operator < (const comp& other) const
	{ 
		return this->len() < other.len();
	}
	bool operator > (const comp& other) const
	{
		return other < *this;
	}
	bool operator == (const comp& other) const
	{
		return !(*this < other || *this > other);
	}
	bool operator != (const comp& other) const
	{
		return !(*this == other);
	}
	bool operator <= (const comp& other) const
	{
		return !(*this > other);
	}

	//����������
	comp& operator ++()
	{
		real = real + 1;
		return *this;
	}
	//�����������
	comp operator ++(int)
	{
		comp temp = *this;
		real = real + 1;
		return temp;
	}
	
	int& operator [](int i)
	{
		if (i == 0) return real;
		return imag;
	}
	//��� ����������� ��������
	const int& operator [](int i) const
	{
		if (i == 0) return real;
		return imag;
	}

	int& operator () ()
	{
		return real;
	}

	// ��� �� ����� ����������� && || ','; ������ ��� �������, ������ ����������. ����� ���������� ��������� ������ ������ �� ����.

	std::vector<int>& operator * ()
	{
		return data;
	}

	//comp& operator ->()
	std::vector<int>& operator ->()
	{
		//return *this;
		return data;
	}

	operator int()
	{
		return this->len();
	}

	// ���������� ��������� ��� ������ ostream, ��� ����, ����� �� ���� �������� � ����� �������� � �������� ���
	friend std::ostream& operator << (std::ostream& out, const comp& object)
	{
		out << object.real << "+" << object.imag << "i";
		return out;
	}

	friend comp operator + (const comp& first, const comp& second)
	{
		comp sum = first;
		return sum += second;
	}
};


int main()
{
	comp c(5,4);
	comp d(6, 7);
	std::cout << c+d;
	
	return 0;
}