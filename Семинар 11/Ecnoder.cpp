#include "Huffman.h"
// !!!��������� ������ ��������� � ������� ������ - � ������ ������ ������� ��������� ����� ������������\�������������� ��������� ������, ������ � ����������� �������. ������� ��������� ���!!!
class bitWriter
{
	long long position;
	vector <byte>* data;
public:
	bitWriter(vector<byte>& data)
	{
		data.clear();
		if (data.size() == 0)
			data.push_back(0);
		this->data = &data;
		position = 0;

	}

	void write(bool bit)
	{
		if (data->size() <= position / 8) data->push_back(0);
		(*data)[position / 8] ^= (bit << (7 - (position % 8)));
		position++;
	}

	long long size()
	{
		return position;
	}
};

class bitReader
{
	long long position;
	const vector <byte>* data;
public:
	bitReader(const vector<byte>& data)
	{
		this->data = &data;
		position = 0;
	}

	bool readNext(bool& bit)
	{
		if (data->size() <= position / 8) return false;
		bit = (*data)[position / 8] & (1 << (7 - (position % 8)));
		position++;
		return true;
	}
};

static void copyStream(IInputStream& input, IOutputStream& output)
{
	vector <byte> bits;
	bitWriter bW(bits);
	byte value;

	while (input.Read(value))
	{
		for (int j = 0; j < 8; j++) bW.write(1 & (value >> j));
	}

	bitReader bR(bits);
	bool current;
	value = 0;

	while (bR.readNext(current))
	{
		value = current;
		for (int j = 1; j < 8; j++)
		{
			bR.readNext(current);
			value = value | (current << j);
		}
		output.Write(value);
	}
}

//void Encode(IInputStream& original, IOutputStream& compressed)
//{
//	copyStream(original, compressed);
//}
//
//void Decode(IInputStream& compressed, IOutputStream& original)
//{
//	copyStream(compressed, original);
//}

#include <iostream> 
#include <queue>
#include <vector>
//#include "Huffman.h"
using byte = unsigned char;

struct Tree {

	Tree* top;
	Tree* right;
	Tree* left;
	int number;
	~Tree() {
		if (right != nullptr) delete right;
		if (left != nullptr) delete left;
	}
};
class CComparator {
public:
	bool operator()(std::pair< int, Tree* > T1, std::pair< int, Tree* > T2) {
		return (T1.first < T2.first);
	}
};
byte from_Bit_To_Byte(std::vector< bool >& w) {
	int b = 0;
	for (int i = 0; i < 8; i++) {
		b += ((w[i]) << (7 - i));
	}
	return byte(b);
}
void write(std::vector< byte >& a, std::vector< std::vector< bool > >& code, IOutputStream& compressed) {
	std::vector < bool > w;
	//������� ������� ��������
	for (int i = 0; i <= 256; i++) {
		if (code[i].size() == 0) {
			continue;
		}
		// ������� ����� ���� �������
		int size = code[i].size();
		for (int j = 7; j >= 0; j--) {
			if ((1 << j) <= size) {
				w.push_back(1);
				size -= (1 << j);
			}
			else {
				w.push_back(0);
			}
			if (w.size() == 8) {
				byte b = from_Bit_To_Byte(w);
				compressed.Write(b);
				w.clear();
			}
		}
		// ������� "������" ��� �������
		int kod = i;
		for (int j = 7; j >= 0; j--) {
			if ((1 << j) <= kod) {
				w.push_back(1);
				kod -= (1 << j);
			}
			else {
				w.push_back(0);
			}
			if (w.size() == 8) {
				byte b = from_Bit_To_Byte(w);
				compressed.Write(b);
				w.clear();
			}
		}
		//��� ������� 
		for (int j = 0; j < code[i].size(); j++) {
			w.push_back(code[i][j]);
			if (w.size() == 8) {
				byte b = from_Bit_To_Byte(w);
				compressed.Write(b);
				w.clear();
			}
		}
	}
	// ����������� �� ��������� ����� � ���������� ��� ���� ������
	if (w.size() != 0) {
		while (w.size() != 8) {
			w.push_back(0);
		}
		byte b = from_Bit_To_Byte(w);
		compressed.Write(b);
		w.clear();
	}
	while (w.size() != 8) {
		w.push_back(0);
	}
	byte b = from_Bit_To_Byte(w);
	compressed.Write(b);
	w.clear();

	//������� �������������� ������������������
	w.clear();
	for (int i = 0; i < a.size(); i++) {
		int number = int(a[i]);
		for (int j = 0; j < code[number].size(); j++) {
			w.push_back(code[number][j]);
			if (w.size() == 8) {
				byte b = from_Bit_To_Byte(w);
				compressed.Write(b);
				w.clear();
			}
		}
	}
	// ������ ������� ������ ����� ������, ������� �� ������������ � 256.
	for (int j = 0; j < code[256].size(); j++) {
		w.push_back(code[256][j]);
		if (w.size() == 8) {
			byte b = from_Bit_To_Byte(w);
			compressed.Write(b);
			w.clear();
		}
	}
	//���� �������� ������ 8 ��������, �� ����� �� ���������� ���� � ������ ���( � decode �� ��� ����� �� ����� ��������� ������ ����� ������� ����� �����).
	if (w.size() != 0) {
		while (w.size() != 8) {
			w.push_back(0);
		}
		byte b = from_Bit_To_Byte(w);
		compressed.Write(b);
		w.clear();
	}

}
void dfs(std::vector< bool >& stack, std::vector< std::vector< bool > > code, Tree* v) {
	if (v->left != nullptr) {
		stack.push_back(true);
		dfs(stack, code, v->left);
	}
	else if (v->right) {
		stack.push_back(false);
		dfs(stack, code, v->right);
	}
	else {
		code[v->number] = stack;
	}
	stack.pop_back();
}
void Encode(IInputStream& original, IOutputStream& compressed) {
	std::vector< byte > a;
	byte b;
	while (original.Read(b)) {
		a.push_back(b);
	}
	std::vector < int > count(257, 0);
	count[256] = 1;
	for (int i = 0; i < a.size(); i++) {
		count[a[i]]++;
	}
	std::priority_queue < std::pair< int, Tree* >, std::vector < std::pair< int, Tree* > >, CComparator  > q;
	for (int i = 0; i <= 256; i++) {
		if (count[i]) {
			Tree* vertex = new Tree;
			vertex->right = nullptr;
			vertex->left = nullptr;
			vertex->number = i;
			q.push({ count[i] , vertex });
		}
	}
	while (q.size() != 1) {
		std::pair< int, Tree* > r1, r2;
		r1 = q.top();
		q.pop();
		r2 = q.top();
		q.pop();
		Tree* vertex = new Tree;
		vertex->left = r1.second;
		vertex->right = r2.second;
		q.push({ r1.first + r2.first ,vertex });
	}
	Tree* tree = new Tree;
	tree = q.top().second;
	std::vector < std::vector< bool > >  code(257);
	std::vector < bool > stack;
	dfs(stack, code, tree->top);
	write(a, code, compressed);
	delete tree->top;
}
void read_alphavit(IInputStream& compressed, std::vector < std::vector < bool > >& code, std::vector < bool >& all) {
	byte b = byte(1);
	while (b != byte(0) && compressed.Read(b)) {
		for (int i = 7; i >= 0; i--) {
			all.push_back((1 << i) & (b));
		}
	}
}
struct Vertex {
	byte symbol;
	Vertex* left = nullptr;
	Vertex* right = nullptr;
	~Vertex() {
		if (right != nullptr) delete right;
		if (left != nullptr) delete left;
	}
	void make_a_vertex(std::vector <bool> t, Vertex* node, int i) {
		for (int j = 0; j < t.size(); j++) {
			if (t[j] == 0) {
				if (node->left == nullptr) {
					node->left = new Vertex;
				}
				node = node->left;
			}
			else {
				if (node->right == nullptr) {
					node->right = new Vertex;
				}
				node = node->right;
			}
		}
		node->symbol = i;
	}
};
void Decode(IInputStream& compressed, IOutputStream& original) {
	std::vector < bool > w, all;
	std::vector < std::vector < bool > > code(257);
	read_alphavit(compressed, code, all);
	for (int i = 0; i < w.size(); ) {
		// �������� size
		int size = 0;
		for (int j = 0; j < 8; j++) {
			size += ((1 << j) * (w[i + j]));
		}
		// �������� �� ����
		i += 8;
		// ���� ����� ������ ����������
		int number = 0;
		for (int j = 0; j < 8; j++) {
			number += ((1 << j) * (w[i + j]));
		}
		// �������� �� ����
		i += 8;
		// ���� ��� �� �������� ������ number
		for (int j = 0; j < size; j++) {
			code[number].push_back(w[i + j]);
		}
		i += size;
	}
	w.clear();
	Vertex* vertex = new Vertex;
	for (int i = 0; i < 256; i++) {
		if (code[i].size() != 0) vertex->make_a_vertex(code[i], vertex, i);
	}
	Vertex* current = vertex;
	byte b;
	int ind = 8;
	while (current != nullptr) {
		if (ind == 8) {
			w.clear();
			compressed.Read(b);
			int r = b;
			for (int j = 7; j >= 0; j--) {
				if (r >= (1 << j)) {
					w.push_back(1);
					r -= (1 << j);
				}
				else {
					w.push_back(0);
				}
			}
			ind = 0;
		}
		if (w[ind] == 0) {
			if (current->left == nullptr) {
				if (current->right != nullptr) return;
				else {
					original.Write(current->symbol);
					current = vertex;
				}
			}
			else {
				current = current->left;
			}
		}
		else {
			if (current->right == nullptr) {
				if (current->left != nullptr) return;
				else {
					original.Write(current->symbol);
					current = vertex;
				}
			}
			else {
				current = current->right;
			}
		}
		ind++;
	}


}
