#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
typedef unsigned char byte;

class IInputStream
{
	const vector<byte>* data;
	unsigned int index;
public:
	IInputStream(const vector<byte>& data)
	{
		this->index = 0;
		this->data = &data;
	}

	bool Read(byte& value)
	{
		if (index >= data->size()) return false;

		value = (*data)[index++];
		return true;
	}
};

class IOutputStream
{
	vector<byte>* data;
public:
	IOutputStream(vector<byte>& data)
	{
		this->data = &data;
	}

	void Write(byte value)
	{
		data->push_back(value);
	}
};

void readFile(vector<byte>& data)
{
	string fileName;
	cout << "File name? Default:test.txt" << endl;
	cin >> fileName;
	ifstream file(fileName);

	char value;
	while (file.get(value))
	{
		data.push_back(value);
	}
}

bool isEqual(const vector<byte>& dataIn, const vector<byte>& dataOut)
{
	if (dataIn.size() != dataOut.size()) {
		return false;
	}

	for (unsigned int i = 0; i < dataIn.size(); i++) {
		if (dataIn[i] != dataOut[i]) {
			return false;
		}
	}

	return true;
}

void Encode(IInputStream& original, IOutputStream& compressed);
void Decode(IInputStream& compressed, IOutputStream& original);

int main()
{
	vector <byte> dataIn;
	vector <byte> compressedData;
	vector <byte> outData;

	readFile(dataIn);

	IInputStream inStreamEn(dataIn);
	IOutputStream outStreamEn(compressedData);
	Encode(inStreamEn, outStreamEn);

	IInputStream inStreamDe(compressedData);
	IOutputStream outStreamDe(outData);
	Decode(inStreamDe, outStreamDe);

	for (unsigned int i= 0; i < dataIn.size(); i++) cout << dataIn[i];
	cout << endl << endl;
	for (unsigned int i = 0; i < outData.size(); i++) cout << outData[i];
	cout << endl << endl;

	if (isEqual(dataIn, outData))
	{
		cout << "equal" << endl << "compress power = " << dataIn.size() / compressedData.size();
	}
	else cout << "not equal" << endl;
	return 0;
}