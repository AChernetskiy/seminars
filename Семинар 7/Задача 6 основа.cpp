#include <iostream>
#include <ctime>
#include <vector>

using namespace std;

void randMassiveGenerate(vector<int>& mass, int lenght)
{
	srand(time(0));
	mass.resize(lenght + 1);

	for (int i = 0; i < lenght; i++)
	{
		mass[i] = rand() * (RAND_MAX + 1) + rand();
	}

}

void sort(vector<int>& mass)
{
	for (int i = 0; i < mass.size(); i++)
	{
		cout << mass[i] << endl;
	}

}

unsigned int calcWorkTime(vector<int>& mass, void (*sortFunct) (vector<int>&))
{
	// Функция clock() вычисляет количество тактов процессора от начала работы программы, затраченное именно вашей программой.
	unsigned int startTime = clock(); // начальное время
	sortFunct(mass);
	unsigned int endTime = clock(); // конечное время
	return endTime - startTime; // искомое время
}

int main()
{
	// Функцию вывода предлагается реализовать самостоятельно. Лучше всего, если это будет csv файл, но решайте сами, главное читаемость
	vector<int> mass({ 1,2,3,4,5 });
	randMassiveGenerate(mass, 100);

	void (*sort1)(vector<int>&) = sort;

	cout << "calcTime = " << calcWorkTime(mass, sort1) << endl;

	return 0;
}