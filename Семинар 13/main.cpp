#include <iostream>

using namespace std;


double f(int num, double n, ...)    
{
	double* p = &n;        
	double sum = 0;
	for (int i = num; i > 0; i--)
	{
		cout << *p << endl;
		sum += (*p);        
		p++;             
	}
	return sum / num;
}

int f(int num, int n, ...)
{
	int* p = &n;
	int sum = 0;
	for (int i = num ; i > 0; i--)
	{
		sum += (*p);
		p++;
	}
	return sum;
}
template <typename T, typename Y> void swap(T, Y)
{
	return;
}

int main()
{
	cout << f(4, 5.3, 5.4, 5.5, 5.6) << endl;

	cout << f(4, 5.3, 5.4, 5.5, 5.6,5.7,5.8,5.5,5.6) << endl;

	cout << f(4, 5, 5, 7, 7) << endl;
	int* a = new int(5);
	int b = 5;

	delete (a);

}