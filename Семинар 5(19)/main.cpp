#include <iostream>

using namespace std;

const unsigned long long Masks[16] = { // ����� ��� ���������� ����� ����� ����� � ����������� �������
	0xF000000000000000,
	0x0F00000000000000,
	0x00F0000000000000,
	0x000F000000000000,
	0x0000F00000000000,
	0x00000F0000000000,
	0x000000F000000000,
	0x0000000F00000000,
	0x00000000F0000000,
	0x000000000F000000,
	0x0000000000F00000,
	0x00000000000F0000,
	0x000000000000F000,
	0x0000000000000F00,
	0x00000000000000F0,
	0x000000000000000F
};

const unsigned long long AntiMasks[16] = { // ����� ��� ��������� �������
	0x0FFFFFFFFFFFFFFF,
	0xF0FFFFFFFFFFFFFF,
	0xFF0FFFFFFFFFFFFF,
	0xFFF0FFFFFFFFFFFF,
	0xFFFF0FFFFFFFFFFF,
	0xFFFFF0FFFFFFFFFF,
	0xFFFFFF0FFFFFFFFF,
	0xFFFFFFF0FFFFFFFF,
	0xFFFFFFFF0FFFFFFF,
	0xFFFFFFFFF0FFFFFF,
	0xFFFFFFFFFF0FFFFF,
	0xFFFFFFFFFFF0FFFF,
	0xFFFFFFFFFFFF0FFF,
	0xFFFFFFFFFFFFF0FF,
	0xFFFFFFFFFFFFFF0F,
	0xFFFFFFFFFFFFFFF0,
};

/*����� ��������, ��������� � ������������� �������� ������ �����.
���������� ������ ���� ������� ��������� ������ (��� , � �� �����, ��� ��� ����� ����� ������ ������ 4 ������� ��� ����������� ���������.*/
class Fifteen
{
	long long data; // ���� �����
	char zeroPosition; // ������� 0, ��� ����, ����� �������� �� ���������.
public:
	Fifteen() : data(0x123456789ABCDEF0), zeroPosition(15) {}
	Fifteen(long long number) : data(number) 
	{
		for (int i = 0; i < 16; i ++)
		{
			if ((data & Masks[i]) == 0)
			{
				zeroPosition = i;
				return;
			}
		}
	}

	bool moveZeroUp()
	{

	}

	bool moveZeroDown()
	{

	}

	bool moveZeroLeft()
	{
		if (zeroPosition % 4 == 0) return false; // ���������, ����� �� �������� ������ ����� �����
		data += (data & Masks[zeroPosition - 1]) >> 4; // �������� �����, ��� ����� ������ �������� ����� ����� �� ������� �����, �������� ��� �� ����� ����� � ���������� 
		data &= AntiMasks[zeroPosition - 1]; // �������� ����� �����
		zeroPosition - 1;
		return true;
	}

	bool moveZeroRight()
	{

	}

	long long getData()
	{
		return data;
	}

	void mix()
	{

	}
};

int main()
{
	Fifteen a;
	cout << std::hex /*������� ������ � 16-����� �������*/ << a.getData() << endl;
	a.moveZeroLeft();
	cout  << std::hex << a.getData() << endl;
	return 0;
}