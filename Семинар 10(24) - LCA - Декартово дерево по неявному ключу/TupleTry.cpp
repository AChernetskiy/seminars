#include <iostream>
#include <vector>
#include <tuple>

/*
Экспериментальный класс для проверки
*/
struct Inject
{
	int d = 0;
	Inject() : d(0) { std::cout << "empty construct" << std::endl; }
	Inject(int in) : d(in) { std::cout << "construct from int" << std::endl; }
	Inject(const Inject& other) : d(other.d) { std::cout << "copy construct" << std::endl; }
	Inject(Inject&& other)noexcept
	{
		std::cout << "move construct" << std::endl;
		d = other.d;
		other.d = 0;
	}

	Inject* operator = (const Inject& other)
	{
		d = other.d;
		return this;
	}

	Inject* operator = (Inject&& other) noexcept
	{
		d = other.d;
		other.d = 0;
		return this;
	}

};

// Для перегрузки template, а точнее частичной специализации шаблонов, требуется выделить главный шаблон, от которого будут отталкиваться остальные
template <int Index, typename a, typename b, typename... Tail>
struct tryStr
{

};

// А это уже специализированный шаблон, где Tail пуст. Но можно сделать и не пустым, главное чтобы он был в конце
template <typename a>
struct tryStr <0, a, int>
{

};
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Tuple будем собирать из самого себя, вторичные классы лишь усложняют реализацию, а по факту пользы мало
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

// базовое объявление, от которого стоит отталкиваться
// Сам Tuple, как известно, создаётся от множественного числа параметров
template <typename... Tail>
class Tuple
{
};
//Основная компонента, из которого и будет рекурсивно собираться класс. Как видим, это частичная специализация для случая, когда длинна Tail >=1 
template <typename Head, typename... Tail>
class Tuple<Head, Tail...> : public Tuple <Tail...>
{
	std::remove_reference_t<Head> data; //Внутренний хранимый тип, с которого снимаются все &, хотя для тренировочного класса думаю это излишне, можно просто от типов создавать
public:
	template <typename HeadCon,typename... TailCon> // Для конструктора будем собирать тип из того типа ссылки, который нам был передан.
	//Для этого используем синтаксис move семантики для forward
	Tuple(HeadCon value, TailCon... values) ://Конструктор вместо Head принимает тип, от которого мы будем конструироваться (а не тип, который будем хранить)
		// В принципе можно и передать тип, из которого будем строить желаемый объект, но лучше не стоит
		Tuple <Tail...>(std::forward<TailCon>(values)...),// Здесь идёт рекурсивное конструирование всех последующих подэлементов. Семантика foo()... значит,
		// Что мы для каждого элемента values используем данную функцию. TailCon так же будет раскрыт в множество элементов
		data(std::forward<HeadCon>(value)) // Конструирование с помощью прямого проброса
	{
	}
	
	// Возврат ссылки на объект. В последствии в get мы преобразуем данный тип в то, что потребуется.
	std::remove_reference_t<Head>& get()
	{
		return data;
	}

	// const вариант
	const std::remove_reference_t<Head>& get() const
	{
		return data;
	}
};
/*
В конце концов после компиляции Tuple в данном варианте раскроется в класс с набором полей, каждое из которых соответствует хранимому типу, как поля из суперклассов.
То есть просто структура как {String a, int b, int c} с той лишь разницей, что собирается она на ходу в процессе компиляции и не нужно создавать целые отдельные типы
*/

//Конец раскрытия рекурсии, так же частичная специализация шаблона. Хотя скорее полная
template <>
class Tuple<>
{
};

template <int Index, typename... Tail> //Базовый шаблон для типа, который поможет узнать, какой тип стоит в Index позиции Tuple
struct TupleType
{};

template <int Index, typename Head, typename... Tail>
struct TupleType<Index, Head, Tail...> // Основной шаг рекурсии
{
	typedef typename TupleType<Index - 1, Tail...>::type type; //Определяем тип (typedef) type, беря имя типа из рекурсивного вызова с Index-1
};

template <typename... Tail>
struct TupleType <0, Tail...>
{
	typedef Tuple<Tail...> type;// И когда мы доходим до конца рекурсии, то забираем весь хвост. Если бы не был объявлен базовый шаблон только с Tail, а был
	//Объявлен базовым именно <Index, Head, Tail>, то мы не смогли бы сделать данный класс, так как параметров не хватает для спецификации (жёсткого определения, чем параметры являются)
	// Почему мы берём не Head? Потому что в get нам нужно превратить Tuple в тот тип, который будет хранить то что нам нужно, а такой тип выглядит именно Tuple<Tail...> с остаточным Tail
};

/*
Пара вариантов get для обычного Tuple lvalue, const lvalue, rvalue. Правда используется читерский метод снимания лишних const и накладывания move на возвращаемый rvalue
*/

template <int Index, typename... Tail>
auto get(Tuple<Tail...>& tuple)
{
	typedef typename TupleType <Index, Tail...>::type RType; //Определяем название типа, в котором хранится нужная переменная
	return (static_cast <RType&> (tuple)).get();// Забираем её с помощью функции get
}

template <int Index, typename... Tail>
const auto get(const Tuple<Tail...>& tuple)
{
	typedef typename TupleType <Index, Tail...>::type RType;
	return (static_cast <const RType&> (tuple)).get();// Мы преобразуем Tuple в const суперкласс и забираем const элемент от туда.
}

template <int Index, typename... Tail>
auto&& get(Tuple<Tail...>&& tuple)
{
	typedef typename TupleType <Index, Tail...>::type RType;
	return std::move((static_cast <RType&> (tuple)).get());// Здесь просто, забираем через T& get, после чего навешиваем move и отдаём rvalue наверх
}


int main()
{
	int d = 2;
	Inject t(3);
	
	Tuple <int, int, Inject> a(2, 1, t); // При конструировании Tuple, можно просмотреть консоль, произойдёт один раз копирование,
	//после чего Inject будет несколько раз перемещён при каждом вызове конструктора суперкласса при раскрытии рекурсии.
	const Tuple <int, int, Inject> b(2, 1, t);

	//t = get<0>(Tuple <Inject, int>(Inject(3), 4));

	get<1>(a); // return int
	get<1>(b); // return int&
	get<1>(Tuple <int, int, Inject>(2, 1, t)); // return int&&

	//std::cout << get<0>(a).d << std::endl;
	//std::get<0>(std::tuple <Inject>(Inject(3)));
	return 0;
}